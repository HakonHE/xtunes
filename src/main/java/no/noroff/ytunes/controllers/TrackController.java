package no.noroff.ytunes.controllers;

import no.noroff.ytunes.models.SongSearch;
import no.noroff.ytunes.repositories.TrackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/")
public class TrackController {
    @Autowired
    private TrackRepository trackRepository;

    public TrackController(TrackRepository trackRepository) { this.trackRepository = trackRepository; }

    @GetMapping
    public String index(Model model) {
        model.addAttribute("SongSearch", new SongSearch());
        model.addAttribute("tracks", trackRepository.getRandomTracks());
        model.addAttribute("artists", trackRepository.getRandomArtists());
        model.addAttribute("genres", trackRepository.getRandomGenres());
        return "homepage-htunes";
    }

    @GetMapping({"search"})
    public String searchResults(Model model, @RequestParam (value = "keyword") String keyword) {
        if(keyword != null) {
            model.addAttribute("songs", trackRepository.getSearchResults(keyword));
        }
        return "search-results-songs";
    }

}
