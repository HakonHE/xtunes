package no.noroff.ytunes.models;

import java.util.Objects;

public class Track {
    private String songName;

    public Track(String songName) {
        this.songName = songName;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    @Override
    public String toString() {
        return "Track{" +
                "songName='" + songName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Track track = (Track) o;
        return Objects.equals(songName, track.songName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(songName);
    }
}
