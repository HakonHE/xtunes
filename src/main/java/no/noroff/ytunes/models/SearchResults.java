package no.noroff.ytunes.models;

import java.util.Objects;

public class SearchResults {
    private String trackName;
    private String artist;
    private String album;
    private String genreName;

    public SearchResults(String trackName, String artist, String album, String genreName) {
        this.trackName = trackName;
        this.artist = artist;
        this.album = album;
        this.genreName = genreName;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    @Override
    public String toString() {
        return "SearchResults{" +
                "trackName='" + trackName + '\'' +
                ", artist='" + artist + '\'' +
                ", album='" + album + '\'' +
                ", genreName='" + genreName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SearchResults that = (SearchResults) o;
        return Objects.equals(trackName, that.trackName) && Objects.equals(artist, that.artist) && Objects.equals(album, that.album) && Objects.equals(genreName, that.genreName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(trackName, artist, album, genreName);
    }
}
