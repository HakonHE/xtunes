package no.noroff.ytunes.models;

public record CustomerCountry(String countryName, int customerCount) {
}
