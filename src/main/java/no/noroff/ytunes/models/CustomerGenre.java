package no.noroff.ytunes.models;

public record CustomerGenre(int customerId, String customerName, int tracksInGenre, String favoriteGenre) {
}
