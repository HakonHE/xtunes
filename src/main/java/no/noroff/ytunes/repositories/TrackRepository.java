package no.noroff.ytunes.repositories;

import no.noroff.ytunes.models.Artist;
import no.noroff.ytunes.models.Genre;
import no.noroff.ytunes.models.SearchResults;
import no.noroff.ytunes.models.Track;

import java.util.List;

public interface TrackRepository {
    List<Artist> getRandomArtists();
    List<Track> getRandomTracks();
    List<Genre> getRandomGenres();
    List<SearchResults> getSearchResults(String searchWord);
}
