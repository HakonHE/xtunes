package no.noroff.ytunes.repositories;

import no.noroff.ytunes.models.Artist;
import no.noroff.ytunes.models.Genre;
import no.noroff.ytunes.models.SearchResults;
import no.noroff.ytunes.models.Track;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TrackRepositoryImpl implements TrackRepository{
    private final String URL = ConnectionHelper.CONNECTION_URL;

    /**
     * Gets five random artists from the database and returns it in a list.
     * @return artistList a list of artists.
     */
    @Override
    public List<Artist> getRandomArtists() {
        List<Artist> artistList = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(URL)) {

            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT name FROM Artist ORDER BY RANDOM() LIMIT 5");

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                artistList.add(new Artist(
                                resultSet.getString("name")
                        )
                );
            }
        } catch (Exception exception) {
            System.out.println(exception.toString());
        }
        return artistList;
    }

    /**
     * Gets five random tracks from the database and returns it in a list.
     * @return trackList a list of tracks.
     */
    @Override
    public List<Track> getRandomTracks() {
        List<Track> trackList = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(URL)) {

            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT name FROM Track ORDER BY RANDOM() LIMIT 5");

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                trackList.add(new Track(
                                resultSet.getString("name")
                        )
                );
            }
        } catch (Exception exception) {
            System.out.println(exception.toString());
        }
        return trackList;
    }

    /**
     * Gets five random genres from the database and returns it in a list.
     * @return genreList a list of genres.
     */
    @Override
    public List<Genre> getRandomGenres() {
        List<Genre> genreList = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(URL)) {

            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT name FROM Genre ORDER BY RANDOM() LIMIT 5");

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                genreList.add(new Genre(
                                resultSet.getString("name")
                        )
                );
            }
        } catch (Exception exception) {
            System.out.println(exception.toString());
        }
        return genreList;
    }

    /**
     * Gets the songs that match the searchWord in the database and returns a list of the results.
     * @param searchWord a string with a searchword.
     * @return searchResultList a list of search results.
     */
    @Override
    public List<SearchResults> getSearchResults(String searchWord) {
        List<SearchResults> searchResultList = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(URL)) {

            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Track.Name, Artist.Name as artistname, Album.Title, Genre.Name as genrename FROM Track\n" +
                            "INNER JOIN Album ON Album.AlbumId=Track.AlbumId\n" +
                            "INNER JOIN Artist ON Artist.ArtistId=Album.ArtistId\n" +
                            "INNER JOIN Genre ON Genre.GenreId=Track.genreid\n" +
                            "Where Track.name LIKE ?");
            preparedStatement.setString(1,"%" + searchWord + "%");

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                searchResultList.add(new SearchResults(
                                resultSet.getString("name"),
                                resultSet.getString("artistname"),
                                resultSet.getString("Title"),
                                resultSet.getString("genrename")
                        )
                );
            }
        } catch (Exception exception) {
            System.out.println(exception.toString());
        }
        return searchResultList;
    }
}
