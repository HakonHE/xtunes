# Htunes

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

A "remake" of itunes, for finding and searching for songs.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)

## Background

This project is the second assignment of the Java Full Stack course. 

#### The task was to create a web application. With the specifications:

* Access the Chinook SQL Lite database through JDBC.
* A Thymeleaf view to show database data.
* This should all be in one project.
* The application must be published as a Docker container on Heroku.

#### The application features are:

* See possible songs, artists and genres on the frontpage.
* Search for songs.
* See a list of the songs matching what you searched for.

## Install

* Install JDK 17
* Install Intellij
* Clone repository

## Usage

* Run the application

## Maintainers

[@HåkonHE](https://gitlab.com/HåkonHE)

## Contributing

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.
